package br.com.hotels.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.hotels.model.HotelsResponse;
import br.com.hotels.services.HotelsServices;
import br.com.hotels.services.HotelsServicesImpl;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws Exception 
	 */
	
	@RequestMapping(value = "/calculate/{cityCode}/{checkin}/{checkout}/{adult}/{child}", method = RequestMethod.GET)
	@ResponseBody
	public List<HotelsResponse> home( @PathVariable String cityCode, 
						@PathVariable String checkin, 
						@PathVariable String checkout, 
						@PathVariable Integer adult, 
						@PathVariable Integer child,
						Locale locale, Model model) throws Exception {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		HotelsServicesImpl hotelsServicesImpl = new HotelsServicesImpl();
		String formattedDate = dateFormat.format(date);
		
		List<HotelsResponse> response = hotelsServicesImpl.test1(cityCode, checkin, checkout, adult, child);
		
		model.addAttribute("serverTime", formattedDate );
		
		return response;
	}
	
	@RequestMapping(value = "/calculate/hotels/{hotel}/{checkin}/{checkout}/{adult}/{child}", method = RequestMethod.GET)
	@ResponseBody
	public HotelsResponse homeHotels( @PathVariable String hotel, 
						@PathVariable String checkin, 
						@PathVariable String checkout, 
						@PathVariable Integer adult, 
						@PathVariable Integer child,
						Locale locale, Model model) throws Exception {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		HotelsServicesImpl hotelsServicesImpl = new HotelsServicesImpl();
		String formattedDate = dateFormat.format(date);
		
		HotelsResponse response = hotelsServicesImpl.test2(hotel, checkin, checkout, adult, child);
		
		model.addAttribute("serverTime", formattedDate );
		
		return response;
	}
}
