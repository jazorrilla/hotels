package br.com.hotels.model;

import java.util.List;

public class ResponeCities {
	private List<Hotel> cities;

	public List<Hotel> getCities() {
		return cities;
	}

	public void setCities(List<Hotel> cities) {
		this.cities = cities;
	}

	@Override
	public String toString() {
		return "ResponeCities [cities=" + cities + "]";
	}
	
}
