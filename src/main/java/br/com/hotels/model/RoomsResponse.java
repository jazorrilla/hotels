package br.com.hotels.model;

public class RoomsResponse {
	private Integer roomID;
    private String categoryName;
    private Double totalPrice;
    private ResponsePriceDetails priceDetail;
	public Integer getRoomID() {
		return roomID;
	}
	public void setRoomID(Integer roomID) {
		this.roomID = roomID;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public ResponsePriceDetails getPriceDetail() {
		return priceDetail;
	}
	public void setPriceDetail(ResponsePriceDetails priceDetail) {
		this.priceDetail = priceDetail;
	}
	
	@Override
	public String toString() {
		return "RoomsResponse [roomID=" + roomID + ", categoryName=" + categoryName + ", totalPrice=" + totalPrice
				+ ", priceDetail=" + priceDetail + "]";
	}
    
}
