package br.com.hotels.model;

import java.util.List;

public class HotelsResponse {
	private Integer id;
	private String cityName;
	private List<RoomsResponse> rooms;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public List<RoomsResponse> getRooms() {
		return rooms;
	}
	public void setRooms(List<RoomsResponse> rooms) {
		this.rooms = rooms;
	}
	@Override
	public String toString() {
		return "HotelsResponse [id=" + id + ", cityName=" + cityName + ", rooms=" + rooms + "]";
	}
	
}
