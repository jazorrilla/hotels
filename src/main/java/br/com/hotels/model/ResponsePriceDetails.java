package br.com.hotels.model;

public class ResponsePriceDetails {
	private Double pricePerDayAdult;
    private Double pricePerDayChild;
    
	public Double getPricePerDayAdult() {
		return pricePerDayAdult;
	}
	public void setPricePerDayAdult(Double pricePerDayAdult) {
		this.pricePerDayAdult = pricePerDayAdult;
	}
	public Double getPricePerDayChild() {
		return pricePerDayChild;
	}
	public void setPricePerDayChild(Double pricePerDayChild) {
		this.pricePerDayChild = pricePerDayChild;
	}
	@Override
	public String toString() {
		return "ResponsePriceDetails [pricePerDayAdult=" + pricePerDayAdult + ", pricePerDayChild=" + pricePerDayChild
				+ "]";
	}
}
