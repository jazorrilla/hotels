package br.com.hotels.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.hotels.dao.HotelsDAO;
import br.com.hotels.model.Hotel;
import br.com.hotels.model.HotelsResponse;
import br.com.hotels.model.ResponeCities;
import br.com.hotels.model.ResponsePriceDetails;
import br.com.hotels.model.Room;
import br.com.hotels.model.RoomsResponse;

@Service
public class HotelsServicesImpl{
	

	public List<HotelsResponse> test1(String cityCode, String checkin, String checkout, Integer adult, Integer child) throws Exception {
		
		HotelsDAO hotelsDao= new HotelsDAO();
		List<HotelsResponse> response = new ArrayList<HotelsResponse>();
		Date date1=null;
		Date date2=null;
		try {
			date1=new SimpleDateFormat("dd-MM-yyyy").parse(checkin);
			date2=new SimpleDateFormat("dd-MM-yyyy").parse(checkout);
		} catch (ParseException e) {
			throw new Exception("Error en lectura de fechas...");
		}
		if (date1.compareTo(date2) > 0) {
			throw new Exception("Rango de fechas invalido...");
        }
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		long days = ChronoUnit.DAYS.between(LocalDate.parse(checkin, formatter), LocalDate.parse(checkout, formatter));
		
		ResponeCities responeCities = hotelsDao.getHotels(cityCode);
		
		if(responeCities.getCities().size() > 0){
			HotelsResponse rs = new HotelsResponse();
			for(Hotel c : responeCities.getCities()) {
				List<RoomsResponse> rms = new ArrayList<RoomsResponse>();
				for(Room r : c.getRooms()) {
					RoomsResponse rm = new RoomsResponse();
					ResponsePriceDetails drm = new ResponsePriceDetails();
					drm.setPricePerDayAdult(r.getPrice().getAdult());
					drm.setPricePerDayChild(r.getPrice().getChild());
					rm.setPriceDetail(drm);
					rm.setCategoryName(r.getCategoryName());
					rm.setRoomID(r.getRoomID());
					rm.setTotalPrice((((r.getPrice().getAdult()*adult)+(r.getPrice().getChild()*child))*days)*0.70);
					rms.add(rm);
				}
				rs.setId(c.getId());
				rs.setCityName(c.getCityName());
				rs.setRooms(rms);
				response.add(rs);
			}
			
		}else{
			throw new Exception("Error al obtener hoteles");
		}
		return response;
	}

	public HotelsResponse test2(String hotCode, String checkin, String checkout, Integer adult, Integer child) throws Exception {
		HotelsResponse rs = new HotelsResponse();
		HotelsDAO hotelsDao= new HotelsDAO();
		Date date1=null;
		Date date2=null;
		try {
			date1=new SimpleDateFormat("dd-MM-yyyy").parse(checkin);
			date2=new SimpleDateFormat("dd-MM-yyyy").parse(checkout);
		} catch (ParseException e) {
			throw new Exception("Error en lectura de fechas...");
		}
		if (date1.compareTo(date2) > 0) {
			throw new Exception("Rango de fechas invalido...");
        }
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		long days = ChronoUnit.DAYS.between(LocalDate.parse(checkin, formatter), LocalDate.parse(checkout, formatter));
		
		Hotel city = hotelsDao.getHotel(hotCode);
		
		if(city != null){
			List<RoomsResponse> rms = new ArrayList<RoomsResponse>();
			for(Room r : city.getRooms()) {
				RoomsResponse rm = new RoomsResponse();
				ResponsePriceDetails drm = new ResponsePriceDetails();
				drm.setPricePerDayAdult(r.getPrice().getAdult());
				drm.setPricePerDayChild(r.getPrice().getChild());
				rm.setPriceDetail(drm);
				rm.setCategoryName(r.getCategoryName());
				rm.setRoomID(r.getRoomID());
				rm.setTotalPrice((((r.getPrice().getAdult()*adult)+(r.getPrice().getChild()*child))*days)*0.70);
				rms.add(rm);
			}
			rs.setId(city.getId());
			rs.setCityName(city.getCityName());
			rs.setRooms(rms);
			
		}else{
			throw new Exception("Error al obtener hotel");
		}
		return rs;
	}
}
