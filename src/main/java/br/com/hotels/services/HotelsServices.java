package br.com.hotels.services;

import java.util.List;

import br.com.hotels.model.HotelsResponse;

public interface HotelsServices {
	public List<HotelsResponse> test1(String cityCode, String checkin, String checkout, Integer adult, Integer child) throws Exception;
}
