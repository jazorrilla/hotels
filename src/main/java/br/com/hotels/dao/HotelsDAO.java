package br.com.hotels.dao;

import java.util.Arrays;

import org.springframework.web.client.RestTemplate;

import br.com.hotels.model.Hotel;
import br.com.hotels.model.ResponeCities;

public class HotelsDAO {
	final String uri1 = "https://cvcbackendhotel.herokuapp.com/hotels/avail/";
	final String uri2 = "https://cvcbackendhotel.herokuapp.com/hotels/";
    
	public ResponeCities getHotels(String code) {
		RestTemplate restTemplate = new RestTemplate();
		Hotel[] cities = restTemplate.getForObject(uri1+code, Hotel[].class);
	    ResponeCities result = new ResponeCities();
	    result.setCities(Arrays.asList(cities));
	    return result;
	}
	public Hotel getHotel(String code) {
		RestTemplate restTemplate = new RestTemplate();
		Hotel[] cities = restTemplate.getForObject(uri2+code, Hotel[].class);
	    return cities[0];
	}
}
